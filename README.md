# javascript web basics

This is not an introduction to the javascript programming language. In this repo I will explpain how javascript runs, either in the browser or with nodejs. How to achieve code splitting. What modules are.


# Project setup

First of all lets build a basic html page. Create an empty folder where our project shall live. The default page a web server will look for in a folder is called `index.html`. Our index.html file looks like this:

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>JS web basics</title>
    </head>
    <body>
        <h1>A Heading</h1>

        <div id="root"></div>

        <script></script>
    </body>
</html>
```

This is a basic html scaffold. The body only contains a heading `<h1>A heading</h1>` and an empty div tag with id `root`. 

## View html in the browser

HTML can be viewed in any web browser. To open the file just double click it in your file browser. This way your browser loads the file from the filesystem. Notice the url bar that shows the filepath prefixed with `file://`. 

## Installing a local fileserver

Loading the HTML file by filesystem only works well for simple html pages with no linking to other files. Later we want to put our javascript code to separate files that will be fetched by the browser. Thus we will need to spin up a small web server.
 
Your IDE might have one included that you can use. I reccomend to install a fileserver via npm. Since we already know how to create npm packages lets do this quickly. Go to the terminal and type `npm init` Choose the default option for all questions:

```terminal
amelch@Laptop-Alf:~/code/js-web-basics$ npm init
This utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sensible defaults.

See `npm help json` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg>` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
package name: (js-web-basics) 
version: (1.0.0) 
description: 
entry point: (index.js) 
test command: 
git repository: 
keywords: 
author: 
license: (ISC) 
About to write to /home/amelch/code/js-web-basics/package.json:

{
  "name": "js-web-basics",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC"
}


Is this OK? (yes) 
amelch@Laptop-Alf:~/code/js-web-basics$ 

```

To install the webserver type `npm install http-server`. This will create a folder named `node_modules` and install the npm packages there. To spin up the server type `npx http-server`.
 
```console
amelch@Laptop-Alf:~/code/js-web-basics$ npm install http-server
npm WARN js-history@1.0.0 No repository field.

+ http-server@0.11.1
added 156 packages from 207 contributors in 2.173s
amelch@Laptop-Alf:~/code/js-web-basics$ npx http-server
Starting up http-server, serving ./
Available on:
  http://127.0.0.1:8080
  http://10.4.6.107:8080
  http://192.168.123.1:8080
Hit CTRL-C to stop the server
```


Note: You can also choose to install npm packages globally. This makes sense for general use packages like http-server. When you do so you can omit the `npx` prefix to use the package in the shell:

- `npm install -g http-server`
- `http-server`

## View the html-page

Go to your webbrowser and navigate to the url shown in the terminal: `http://127.0.0.1:8080`. You can see our basic web page:

- Todo: Insert img

Open your browsers dev tools to inspect how your browser interprets the served files. Under "Console" tab you get a interactive Javascript console. Javascript is a interpreted language. Meaning you can interpret the code line by line without having to compile the whole codebase. If you want to test simple javascript expressions the Console is the place to go. Try it out. Declare variables and print them out via `console.log()`.

## Javascript in html

Lets write our first lines of code that will be served to the user. You can write javascript code into your html-document by enclosing it in `script`-tags like so:

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Mein Seite</title>

        <script>
            console.log('Hello World')
            
            var i = 15
            
            i += 7
            
            console.log(i)
        </script>
    </head>
    <body>
        <h1>Test</h1>

        <div id="root"></div>

    </body>
</html>
```

Save the document and reload the browser to see the console output in dev tools.

Now lets modify the DOM (Document Object Model):

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Mein Seite</title>
    </head>
    <body>
        <h1>Test</h1>

        <div id="root"></div>
        
        <script>
            var meinELement = document.createElement('div')
            meinElement.innerHTML = 'Hallo Welt!'
            
            document.getElementById('root').appendChild(meinElement)
        </script>
        
    </body>
</html>
```

Save and reload. Our root div now contains a new Element. Take a look at the dev tools:

- Todo: Insert img

Note: About the location of scripts. Notice how I used two different locations for the script tag. In our second example we required the page to be already loaded to access the root div (`document.getElementById('root')`). 

Try it yourself: Place the script above at different positions and examine the errors you get.

As a rule of thumb. Code that provides us functionality like vendor packages (e.g. jQuery) is placed in the head. Code that maipulated DOM is placed at the end of the body block. This is best shown in teh following example:

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Mein Seite</title>
        
        <script>
            function sum(x, y) {
                return x + y
            }
        </script>
    
    </head>
    <body>
        <h1>Test</h1>

        <div id="root"></div>
        
        <script>
            var meinELement = document.createElement('div')
            meinElement.innerHTML = 'Hallo Welt!'
            
            document.getElementById('root').appendChild(meinElement)
        
            console.log(sum(5, 7))
        </script>
        
    </body>
</html>
```

The function sum has to be declared before its use.

## Work in progress

This tutorial is not finished. I will continue here when i have the time.
